#!/usr/bin/env bash

#./gradlew test --rerun-tasks --tests "com.pera.exercises.TestLRUCache.test0*"
#./gradlew test --rerun-tasks --tests "com.pera.exercises.TestArrayBinarySearch"

./gradlew test --info --rerun-tasks --tests "com.pera.exercises.Test*"
