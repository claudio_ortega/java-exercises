package com.pera.exercises;

import org.junit.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static com.pera.util.SystemUtil.getCurrentDirectory;

public class TestAnything 
{
    static void checkCondition( boolean condition ) {
        if ( ! condition ) {
            throw new RuntimeException("");
        }
    }

    @Test
    public void testLRUCache()
    {
        final LRUCache cache = new LRUCache(3);

        cache.set( "1", "a" );
        cache.set( "2", "b" );
        cache.set( "3", "c" );
        checkCondition( cache.get( "4" ) == null );

        cache.set( "4", "d" );
        checkCondition( cache.get( "4" ) != null );

        cache.delete( "4" );
        checkCondition( cache.get( "4" ) == null );
    }

    @Test
    public void testArrayRepetition()
    {
        EliminateRepetitionsInArray.main(new String[]{});
    }

    @Test
    public void testSomething() throws Exception
    {
        System.out.println( getCurrentDirectory() );
        File inputFile = new File( "./input.txt" );
        final BufferedReader br = new BufferedReader( new InputStreamReader( new FileInputStream( inputFile ), StandardCharsets.US_ASCII) );
        String line;
        while ( ( line = br.readLine() ) != null )
        {
            if ( line.length() > 0 ) {
                System.out.println("[" + line + "]");

                System.out.println( "HTTP/1.0 200 OK" );
                // 'GET /sum?a=1&b=12&c=100 HTTP/1.0';  --> 1+12+100=113
                // a=1&b=12&c=100
                // { a=1, b=12, c=100 }
                final Integer result = parseAndCompute("GET /sum?a=1&b=12&c=100 HTTP/1.0");
                if ( result == null ) {
                    System.out.println( "error" );
                }
                System.out.println( String.format("%d", result) );
            }
        }

        br.close();
        System.out.println("done");
    }

    /*
        input: "GET /sum?a=1&b=12&c=100 HTTP/1.0"
        output Integer(113)
    */
    private static Integer parseAndCompute( String in ) {

        String []firstPass = in.split(" ");
        String middlePortion = firstPass[1];             // /sum?a=1&b=12&c=100
        String []secondPass = middlePortion.split("\\?");
        String innerPortion = secondPass[1];             // a=1&b=12&c=100
        String []thirdPass = innerPortion.split("\\&");

        int summation = 0;
        for ( String nextEquality : thirdPass ) {
            System.out.println( "nextEquality:[" + nextEquality + "]" );
            String []equality = nextEquality.split("\\=");
            Integer value = Integer.valueOf( equality[1] );
            summation = summation + value;
        }

        return summation;
    }
}




