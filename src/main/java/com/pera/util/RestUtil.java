/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import org.apache.http.*;
import org.apache.http.client.fluent.*;
import org.apache.http.client.utils.*;
import org.apache.http.entity.*;

import java.io.*;
import java.nio.charset.*;

public class RestUtil
{
    public static RestReturn sendGet (
        String protocol,
        String serverName,
        int serverPort,
        String aInOperation,
        int timeoutMSec )
            throws Exception
    {
        final Request request = Request.Get (
            new URIBuilder()
                .setScheme ( protocol )
                .setHost ( serverName )
                .setPort ( serverPort )
                .setPath ( aInOperation )
                .build ()
                .toString ()
        );

        request.socketTimeout( timeoutMSec );
        request.connectTimeout( timeoutMSec );

        final HttpResponse lRet = request
            .execute()
            .returnResponse();

        return new RestReturn ( ).setStatus ( getStatusCode ( lRet ) ).setContent ( getContent ( lRet ) );
    }

    public static <T> RestReturn sendPut (
        String protocol,
        String serverName,
        int serverPort,
        String aInOperation,
        int timeoutMSec,
        T aInObject )
        throws Exception
    {
        final String stBody;

        if ( aInObject instanceof String ) {
            stBody = aInObject.toString();
        } else {
            stBody = GsonUtil.serialize ( aInObject );
        }

        final Request request = Request.Put (
            new URIBuilder ()
                .setScheme ( protocol )
                .setHost ( serverName )
                .setPort ( serverPort )
                .setPath ( aInOperation )
                .build ()
                .toString ()
        );

        request.socketTimeout( timeoutMSec );
        request.connectTimeout( timeoutMSec );

        final HttpResponse lRet = request
            .body ( new StringEntity( stBody ) )
            .execute ()
            .returnResponse();

        return new RestReturn ( ).setStatus ( getStatusCode ( lRet ) ).setContent ( getContent ( lRet ) );
    }

    public static <T> RestReturn sendPost (
        String protocol,
        String serverName,
        int serverPort,
        String aInOperation,
        int timeoutMSec,
        T aInObject )
        throws Exception
    {
        final String stBody;

        if ( aInObject instanceof String ) {
            stBody = aInObject.toString();
        } else {
            stBody = GsonUtil.serialize ( aInObject );
        }

        final Request request = Request.Post (
            new URIBuilder ()
                .setScheme ( protocol )
                .setHost ( serverName )
                .setPort ( serverPort )
                .setPath ( aInOperation )
                .build ()
                .toString ()
        );

        request.socketTimeout( timeoutMSec );
        request.connectTimeout( timeoutMSec );

        final HttpResponse lRet = request
            .body ( new StringEntity ( stBody ) )
            .execute ()
            .returnResponse();

        return new RestReturn ( ).setStatus ( getStatusCode ( lRet ) ).setContent ( getContent ( lRet ) );
    }

    private static int getStatusCode ( HttpResponse aInResponse ) {
        return aInResponse.getStatusLine ().getStatusCode ();
    }

    private static String getContent ( HttpResponse aInResponse ) throws IOException
    {
        final StringBuilder sb = new StringBuilder (  );

        try ( InputStream inStream = aInResponse.getEntity ().getContent () )
        {
            final byte[] buffer = new byte[10000];

            int n;
            while (  ( n = inStream.read ( buffer ) ) != -1 )
            {
                sb.append ( new String ( buffer, 0, n, Charset.forName("UTF-8") ) );
            }
        }

        return sb.toString ();
    }

    public static class RestReturn
    {
        private int status;
        private String content;

        private RestReturn ()
        {
            status = -1;
            content = "";
        }

        public int getStatus ()
        {
            return status;
        }

        public String getContent ()
        {
            return content;
        }

        private RestReturn setStatus ( int aInStatus )
        {
            status = aInStatus;
            return this;
        }

        private RestReturn setContent ( String aInContent )
        {
            content = aInContent;
            return this;
        }

        @Override public String toString ()
        {
            return "RestReturn{" +
                "status=" + status +
                ", content='" + content + '\'' +
                '}';
        }
    }
}
