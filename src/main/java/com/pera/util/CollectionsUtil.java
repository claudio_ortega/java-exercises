/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionsUtil
{
    // you shall not instantiate this class
    private CollectionsUtil() {}

    @SafeVarargs
    public static <T> Collection<T> concatenateCollections( Collection <T> ... aInCollections )
    {
        return concatenateCollections ( Arrays.asList(aInCollections) );
    }

    public static <T> Collection<T> createCollection( Collection <T>  aInCollection )
    {
        return concatenateCollections ( aInCollection );
    }

    public static <T> Collection<T> concatenateCollections( Collection< Collection <T> > aInACollectionOfCollections )
    {
        final List<T> lReturnList = new LinkedList<>();

        for ( Collection<T> lNext : aInACollectionOfCollections )
        {
            lReturnList.addAll( lNext );
        }

        return lReturnList;
    }

    @SafeVarargs
    public static <T> List <T> createList( T ... a )
    {
        return new LinkedList<>( Arrays.asList( a ) );
    }

    public static <T> boolean containsElement(
            Comparator<T> aInComparator,
            Collection<T> aInCollection,
            T aInElement )
    {
        boolean lRet = false;

        for ( T next : aInCollection )
        {
            if ( 0 == aInComparator.compare( next, aInElement ) )
            {
                lRet = true;
                break;
            }
        }

        return lRet;
    }

    @SafeVarargs
    public static <T> List <T> concatenateLists( List<T> ... a )
    {
        List<T> lReturnList = new LinkedList<>();

        for ( Collection<T> lNext : a )
        {
            lReturnList.addAll( lNext );
        }

        return lReturnList;
    }

    @SafeVarargs
    public static <T> Set <T> concatenateSets( Set<T> ... a )
    {
        Set<T> lReturnSet= new HashSet<>();

        for ( Collection<T> lNext : a )
        {
            lReturnSet.addAll( lNext );
        }

        return lReturnSet;
    }

    /**
     * Returns a new list with the contents of the given list after adding the new elements.  The
     * original list passed in is unaltered.
     */
    @SafeVarargs
    public static <T> List <T> addToList( List <T> l, T ... a )
    {
        final List<T> lReturnList = new LinkedList<>(l);
        Collections.addAll( lReturnList, a );
        return lReturnList;
    }

    public static <T> List <T> createList( Collection<T> a )
    {
        return new LinkedList<>( a );
    }

    public static <T> List <T> createImmutableList( Collection<T> a )
    {
        return Collections.unmodifiableList( createList( a ));
    }

    public static <T> Set <T> createSet( Collection<T> a )
    {
        return new HashSet<>( a );
    }

    @SafeVarargs
    public static <T> List <T> createImmutableList( T ... a )
    {
        return Collections.unmodifiableList( createList( a ) );
    }

    @SafeVarargs
    public static <T> Set <T> createSet( T ... a )
    {
        return new HashSet<>( Arrays.asList(a) );
    }

    @SafeVarargs
    public static <T> Set <T> createImmutableSet( T ... a )
    {
        return Collections.unmodifiableSet( createSet( a ) );
    }

    public static <T1,T2> Map <T1,T2> createMap( Collection<Map.Entry<T1,T2>> aInEntrySet )
    {
        Map<T1,T2> lReturn = new HashMap<>();

        for( Map.Entry<T1,T2> lNextEntry : aInEntrySet )
        {
            lReturn.put ( lNextEntry.getKey(), lNextEntry.getValue() );
        }

        return lReturn;
    }

    @SafeVarargs
    public static <T1,T2> Map <T1,T2> createMap( Map.Entry<T1,T2> ... a )
    {
        return createMap ( CollectionsUtil.createList( a ) );
    }

    @SafeVarargs
    public static <T1,T2> Map <T1,T2> createInmutableMap( Map.Entry<T1,T2> ... a )
    {
        return Collections.unmodifiableMap ( createMap ( a ) );
    }

    public static <T> List<T> addListsWithNoRepetition(
            List<T> aInListA,
            List<T> aInListB )
    {
        final Set<T> lSet = new HashSet<>( );

        lSet.addAll( aInListA );
        lSet.addAll( aInListB );

        return new LinkedList<>( lSet );
    }

    public static String getString(
        Map<String,String> map,
        String key,
        String defaultValue ) throws IllegalArgumentException {

        String st = map.get(key);

        if (st == null) {
            st = defaultValue;
        }

        if (st == null) {
            throw new IllegalArgumentException("key: " + key + ", value is null");
        }

        return st;
    }

    public static String getTrimmedString(
        Map<String,String> map,
        String key,
        String defaultValue ) throws IllegalArgumentException {
            return getString( map, key, defaultValue ).trim();
    }

    public static Integer getInteger(
        Map<String,String> map,
        String key,
        Integer defaultValue ) throws IllegalArgumentException {
        try {
            final String st = defaultValue == null
                ? getTrimmedString (map, key, null )
                : getTrimmedString (map, key, defaultValue.toString() );
            return Integer.valueOf(st);
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    public static Boolean getBoolean(
        Map<String,String> map,
        String key,
        Boolean defaultValue ) throws IllegalArgumentException {
        try {
            final String st = defaultValue == null
                ? getTrimmedString (map, key, null )
                : getTrimmedString (map, key, defaultValue.toString() );
            return Boolean.valueOf(st);
        } catch (Exception ex) {
            return defaultValue;
        }
    }
    
    public static Float getFloat(
        Map<String,String> map,
        String key,
        Float defaultValue ) throws IllegalArgumentException {
        try {
            final String st = defaultValue == null
                ? getTrimmedString (map, key, null )
                : getTrimmedString (map, key, defaultValue.toString() );
            return Float.valueOf(st);
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    public interface KeyExtractor<T>
    {
        String getKey( T t );
    }

    /**
     *
     * @param aInBaseList
     * @param aInListToAdd
     * @param aInKeyExtractor
     * @param <T>
     * @return
     */
    public static <T> List<T> concatenateListsWithNoRepeatedKey(
            List<T> aInBaseList,
            List<T> aInListToAdd,
            KeyExtractor<T> aInKeyExtractor )
    {
        final List<T> lConcatenatedList = CollectionsUtil.concatenateLists ( aInBaseList, aInListToAdd );

        final Set<String> lMonitoringSet = new HashSet<>( );

        final List<T> lResultList = new LinkedList<>();

        for ( T lNextCandidate : lConcatenatedList )
        {
            final String lKey = aInKeyExtractor.getKey ( lNextCandidate );

            if ( ! lMonitoringSet.contains( lKey ) )
            {
                lResultList.add( lNextCandidate );
            }

            lMonitoringSet.add( lKey );
        }

        return lResultList;
    }

    /**
     *
     * @param aInList   { a,b,b,c,c,d,e }
     * @param <T>
     * @return          { b,c }
     */
    public static <T> Set<T> crateSetFromRepeatedElements( List<T> aInList )
    {
        final Set<T> lMonitoringSet = new HashSet<>( );

        final Set<T> lReturnSet = new HashSet<>();

        for ( T lNext : aInList )
        {
            if ( lMonitoringSet.contains( lNext ) )
            {
                lReturnSet.add( lNext );
            }

            lMonitoringSet.add( lNext );
        }

        return lReturnSet;
    }

    public static <T> void addToListWithNoRepetition(
            List<T> aInOutList,
            T aInToBeAdded )
    {
        if ( ! aInOutList.contains( aInToBeAdded ) )
        {
            aInOutList.add( aInToBeAdded );
        }
    }

    /**
     *
     * @param aInOutList      should not be null
     * @param aInToBeAdded    should not be null
     * @param aInComparator   should return 0 when itens are considered equal
     * @param <T>
     * @return                true iff the item was added to the list
     */
    public static <T> boolean addToListWithNoRepetition(
            List<T> aInOutList,
            T aInToBeAdded,
            Comparator<T> aInComparator )
    {
        boolean aInMatchFound = false;

        for ( T lNext : aInOutList )
        {
            if ( aInComparator.compare( lNext, aInToBeAdded ) == 0)
            {
                aInMatchFound = true;
                break;
            }
        }

        if ( ! aInMatchFound )
        {
            aInOutList.add( aInToBeAdded );
        }

        return ! aInMatchFound;
    }

    public static <T> void addListAIntoBWithNoRepetition(
            List<T> aInOutListA,
            List<T> aInOutListB)
    {
        for ( T lNext : aInOutListA )
        {
            addToListWithNoRepetition( aInOutListB, lNext );
        }
    }

    public interface ITransformer<In,Out>
    {
        Out transform( In aInS );
    }

    public static class NullTransformer <T> implements ITransformer<T,T>
    {
        private NullTransformer() {}

        @Override
        public T transform ( T aInT )
        {
            return aInT;
        }
    }

    public static final ITransformer NULL_TRANSFORMER = new NullTransformer();

    public static <In,Out> Set<Out> transformSet(
            Set<In> aInSet,
            ITransformer<In,Out> aInTransformer )
    {
        final Set<Out> lOut = new HashSet<>();

        for ( In lNextIn : aInSet )
        {
            final Out lOutElement  = aInTransformer.transform( lNextIn );

            if ( null != lOutElement )
            {
                lOut.add( lOutElement );
            }
        }

        return lOut;
    }

    public static <In,Out> List<Out> transformList(
            List<In> aInList,
            ITransformer<In,Out> aInTransformer )
    {
        List<Out> lOut = new LinkedList<>();

        for ( In lNextIn : aInList )
        {
            Out lOutElement  = aInTransformer.transform( lNextIn );

            if ( null != lOutElement )
            {
                lOut.add( lOutElement );
            }
        }

        return lOut;
    }

    public static <T> List<T> filterList(
            List<T> aInList,
            IFilter<T> aInFilter )
    {
        final List<T> lResult = new LinkedList<>();

        for ( T lNext : aInList )
        {
            if ( aInFilter.doesPass( lNext ) )
            {
                lResult.add( lNext );
            }
        }

        return lResult;
    }

    public static <T> boolean compareSets(
            Set<T> aInSetA,
            Set<T> aInSetB)
    {
        return aInSetA.equals( aInSetB );
    }

    public static <T> Set<T> getSetUnion(
            Set<T> aInSetA,
            Set<T> aInSetB )
    {
        final Set<T> lUnion = new HashSet<>();

        lUnion.addAll( aInSetA );
        lUnion.addAll( aInSetB );

        return lUnion;
    }

    public static <T> Set<T> getSetIntersection(
            Set<T> aInSetA,
            Set<T> aInSetB )
    {
        final Set<T> lSideA = new HashSet<>( aInSetA );

        lSideA.retainAll( aInSetB );

        return lSideA;
    }

    public static <T> boolean isSetAIncludedInSetB(
            Set<T> aInSetA,
            Set<T> aInSetB )
    {
        final Set<T> lSideA = new HashSet<>( aInSetA );

        lSideA.removeAll( aInSetB );

        return lSideA.isEmpty();
    }

    public static <T> Set<T> getSetSymmetricalDiff(
            Set<T> aInSetA,
            Set<T> aInSetB )
    {
        final Set<T> lSideA = new HashSet<>( aInSetA );
        final Set<T> lSideB = new HashSet<>( aInSetB );

        lSideA.removeAll( aInSetB );
        lSideB.removeAll( aInSetA );

        return getSetUnion( lSideA, lSideB );
    }

    public static <T> Set<T> getSetAMinusSetB(
            Set<T> aInSetA,
            Set<T> aInSetB )
    {
        final Set<T> lSideA = new HashSet<>( aInSetA );

        lSideA.removeAll( aInSetB );

        return lSideA;
    }

    public static <T> boolean campareLists( List<T> aInListA, List<T> aInListB )
    {
        boolean lRet = true;

        if ( aInListA.size () != aInListB.size() )
        {
            lRet = false;
        }

        else
        {
            for ( int i = 0; i < aInListA.size ( ); i++ )
            {
                if ( ! aInListA.get ( i ).equals ( aInListB.get ( i ) ) )
                {
                    lRet = false;
                    break;
                }
            }
        }

        return lRet;
    }

    public static <T> boolean compareArrays ( T[] a, T[] b )
    {
        return campareLists ( Arrays.asList ( a ), Arrays.asList ( b ) );
    }

    @SafeVarargs
    public static <T> T[] concatArrays ( T[] ... args )
    {
        T[] result = null;

        for ( T[] next : args )
        {
            if ( result == null )
            {
                result = next;
            }
            else
            {
                result = concatTwoArrays ( result, next );
            }
        }

        return result;
    }

    public static <T> T[] concatTwoArrays ( T[] A, T[] B )
    {
        T[] result = Arrays.copyOf( A, A.length + B.length );
        System.arraycopy( B, 0, result, A.length, B.length );
        return result;
    }

    public static <T> void copyArrayFromAToB ( T[] A, T[] B )
    {
        Preconditions.checkArgument ( A.length == B.length );
        System.arraycopy( A, 0, B, 0, A.length );
    }

    @SafeVarargs
    public static <T> T[] appendToArray( T[] A, T ... args )
    {
        return concatArrays( A, args );
    }

    public static <T> T[] createArrayFromList(List<T> aInList)
    {
        return (T[]) aInList.toArray();
    }

    public static <T> int getNumberOfNullsInCollection( Collection<T> aInCollection )
    {
        int lCountNulls = 0;

        for ( T lNext : aInCollection )
        {
            if ( lNext == null )
            {
                lCountNulls++;
            }
        }

        return lCountNulls;
    }

    public static String stringifyCollectionSize( Collection aInCollection )
    {
        return aInCollection == null ? "null collection" : Integer.toString( aInCollection.size() );
    }

    public static String stringifyMapSize( Map aInMap )
    {
        return aInMap == null ? "null map" : Integer.toString( aInMap.size() );
    }

    public static boolean safeEquals ( Object aInObjectAOrNull, Object aInObjectBOrNull )
    {
        final boolean lReturn;

        if ( aInObjectAOrNull == null || aInObjectBOrNull == null )
        {
            lReturn = aInObjectAOrNull == null && aInObjectBOrNull == null;
        }
        else
        {
            lReturn = aInObjectAOrNull.equals ( aInObjectBOrNull );
        }

        return lReturn;
    }
}
