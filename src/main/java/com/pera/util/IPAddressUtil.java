/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.Preconditions;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class IPAddressUtil
{
    private IPAddressUtil()
    {
    }

    public enum InetAddrType
    {
        ipv4("IPv4"), ipv6("IPv6"), unknown("unknown");

        private final String value;

        InetAddrType( String aInValue )
        {
            value = aInValue;    
        }
        
        public String toString()
        {
            return value;
        }
    }

    /**
     *
     * @param aInAddressWithLength   1.2.3.4/25   
     * @return                       true if "/" is in there
     */
    public static boolean hasAddressAndLengthIPv4AndIPv6(String aInAddressWithLength)
    {
        return aInAddressWithLength.contains( "/" );
    }
    
    /**
     *
     * @param aInAddressWithLength   1.2.3.4/25   
     * @return                       true if the IPAddress is proper and also the host portion is all zeroes
     */
    public static boolean validateAddressAndLength( String aInAddressWithLength )
    {
        boolean lReturn = false;
        
        if ( aInAddressWithLength.contains( "/" ) )
        {
            String lAddressPortion = StringUtil.splitOverString( aInAddressWithLength, "/" ).get( 0 );
        
            if ( isPossiblyAnIpv4Address( lAddressPortion ) )
            {
                String lLengthAsString = StringUtil.splitOverString( aInAddressWithLength, "/" ).get( 1 );

                try
                {
                    int lMaskLength = Integer.parseInt( lLengthAsString );
                    
                    if( lMaskLength >= 0 && lMaskLength <= 32 )
                    {
                        // check if the host portion is all zeroes
                        long lAddressAsNumber = IPAddressUtil.getNumberFromIPv4DotNotation( lAddressPortion );
                        long lNumberOfZeros = 32 - lMaskLength;
                        long lNumberWithAllOnesInTheHostPart = ( 1 << lNumberOfZeros ) - 1;
                        
                        lReturn = ( lNumberWithAllOnesInTheHostPart & lAddressAsNumber ) == 0; 
                    }
                }
                catch ( NumberFormatException ex )
                {
                    // ignore, the result is in lReturn=false;                    
                }
            }
        }
        
        return lReturn;
    }

    /**
     * use only if hasAddressAndLengthIPv4AndIPv6() returns true
     * @param aInAddressWithLength   1.2.3.4/25
     * @return                       1.2.3.4
     */
    public static String getAddressFromAddressAndLengthIPv4AndIPv6(String aInAddressWithLength)
    {
        return StringUtil.splitOverString( aInAddressWithLength, "/" ).get( 0 );
    }

    /**
     * use only if hasAddressAndLengthIPv4AndIPv6() returns true
     * @param aInAddressWithLength   1.2.3.4/25
     * @return                       25
     */
    public static String getLengthFromAddressAndLengthIPv4AndIPv6(String aInAddressWithLength)
    {
        return StringUtil.splitOverString( aInAddressWithLength, "/" ).get( 1 );
    }

    /**
     * use only if hasAddressAndLengthIPv4AndIPv6() returns true
     * @param aInAddressWithLength   1.2.3.4/25
     * @return                       255.255.255.128
     */
    public static String getIPv4NetworkMaskFromAddressAndLength(String aInAddressWithLength)
    {
        String lLengthAsString = StringUtil.splitOverString( aInAddressWithLength, "/" ).get( 1 );
        return getIPv4NetworkMaskFromLength( lLengthAsString );
    }

    /**
     * @param aInMaskLength   25
     * @return                255.255.255.128
     */
    public static String getIPv4NetworkMaskFromLength(String aInMaskLength)
    {
        int lLengthAsInteger = Integer.parseInt( aInMaskLength );

        long lMaskAsANumber = getIPv4NetMaskNumber( lLengthAsInteger );

        String lReturn = "";

        for ( int i=0; i<4; i++ )
        {
            long lOctet = lMaskAsANumber % 256;

            lMaskAsANumber = lMaskAsANumber/256;

            if ( i != 0 )
            {
                lReturn = "." + lReturn;
            }

            lReturn = lOctet + lReturn;
        }

        return lReturn;
    }

    public static long getIPv4NetMaskNumber(long aInMaskLength)
    {
        Preconditions.checkArgument( aInMaskLength >= 0L && aInMaskLength < 33L );

        long lRet = 0;
        long lWalkingOne = 1L << 31;

        for ( int i=0; i<aInMaskLength; i++ )
        {
            lRet = lRet + ( lWalkingOne >> i );
        }

        return lRet;
    }
    /**
     *
     * @param aInTestHostIPA        1.2.3.4
     * @param aInTestNetworkB       1.2.0.0
     * @param aInNetworkMaskB       255.255.0.0 (inverted=false) 0.0.255.255 (inverted=true)
     * @param aInMaskIsInvertedInB  255.255.0.0 (aInMaskIsInverted=false)
     *                              0.0.255.255 (aInMaskIsInverted=true)
     *
     * @return
     */
    public static boolean isIPv4HostIPAASubnetOfB(
        String aInTestHostIPA,
        String aInTestNetworkB,
        String aInNetworkMaskB,
        boolean aInMaskIsInvertedInB)
    {
        return isIPv4AndMaskAASubnetOfB(
            aInTestHostIPA,
            "255.255.255.255",
            aInTestNetworkB,
            aInNetworkMaskB,
            aInMaskIsInvertedInB );
    }

    public static boolean isIPv4AndMaskASameNetworkAsB(
        String aInNetworkA,
        String aInNetworkB,
        String aInNetworkMaskForBoth)
    {
        return isIPv4AndMaskAASubnetOfB(
            aInNetworkA,
            aInNetworkMaskForBoth,
            aInNetworkB,
            aInNetworkMaskForBoth,
            false );
    }

    /**
     *
     * @param aInNetworkA           1.2.3.4
     * @param aInNetworkMaskA       255.255.255.0
     * @param aInNetworkB           1.2.0.0
     * @param aInNetworkMaskB       255.255.0.0 (inverted=false) 0.0.255.255 (inverted=true)
     * @param aInMaskIsInvertedInB  255.255.0.0 (aInMaskIsInverted=false)
     *                              0.0.255.255 (aInMaskIsInverted=true)
     *
     * @return
     */
    public static boolean isIPv4AndMaskAASubnetOfB(
        String aInNetworkA,
        String aInNetworkMaskA,
        String aInNetworkB,
        String aInNetworkMaskB,
        boolean aInMaskIsInvertedInB)
    {
        long lNetworkAAsNumber = getNumberFromIPv4DotNotation( aInNetworkA );
        long lNetworkAMaskAsNumber = getNumberFromIPv4DotNotation( aInNetworkMaskA );

        long lNetworkBAsNumber = getNumberFromIPv4DotNotation( aInNetworkB );
        long lNetworkBMaskAsNumber = getNumberFromIPv4DotNotation( aInNetworkMaskB );
        if ( aInMaskIsInvertedInB )
        {
            lNetworkBMaskAsNumber = ~lNetworkBMaskAsNumber;
        }

        return isIPv4AndLengthAASubnetOfB(
            lNetworkAAsNumber,
            lNetworkAMaskAsNumber,
            lNetworkBAsNumber,
            lNetworkBMaskAsNumber );
    }

    public static String getNetworkPortionFromIPv4AddressAndLength(
        String aInAddress,
        String aInLength)
    {
        return getIPv4DotNotationFromNumber( getNetworkPortionFromIPv4AddressAndLengthAsNumber( aInAddress, aInLength ) );
    }

    public static long getNetworkPortionFromIPv4AddressAndLengthAsNumber(
        String aInAddress,
        String aInLength)
    {
        final long lAddressAsNumber = getNumberFromIPv4DotNotation( aInAddress );
        final long lNetworkMaskAsNumber = getIPv4NetMaskNumber( Integer.parseInt( aInLength ) );
        return lAddressAsNumber & lNetworkMaskAsNumber;
    }

    public static long getHostPortionFromIPv4AddressAndLengthAsNumber(
        String aInAddress,
        String aInLength)
    {
        final long lAddressAsNumber = getNumberFromIPv4DotNotation( aInAddress );
        final long lNetworkMaskAsNumber = getIPv4NetMaskNumber( Integer.parseInt( aInLength ) );
        return lAddressAsNumber & ~lNetworkMaskAsNumber;
    }

    public static boolean isZeroIPv4Address( String aInTestIP )
    {
        return "0.0.0.0".equals( aInTestIP );
    }

    /**
     *
     * @param aInTestHostIPA                1.2.3.4
     * @param aInTestNetworkAndSIDRB        1.2.0.0/16
     * @return
     */
    public static boolean isHostIPv4AASubnetOfB(
        String aInTestHostIPA,
        String aInTestNetworkAndSIDRB)
    {
        return
            isIPv4AndMaskAASubnetOfB(
                aInTestHostIPA + "/32",
                aInTestNetworkAndSIDRB );
    }

    /**
     *
     * @param aInTestNetworkAndSIDRA  1.2.3.0/24
     * @param aInTestNetworkAndSIDRB  1.2.0.0/16
     * @return
     */
    public static boolean isIPv4AndMaskAASubnetOfB(
        String aInTestNetworkAndSIDRA,
        String aInTestNetworkAndSIDRB)
    {
        List<String> lListA =  StringUtil.splitOverString( aInTestNetworkAndSIDRA, "/" );
        List<String> lListB =  StringUtil.splitOverString( aInTestNetworkAndSIDRB, "/" );

        Preconditions.checkArgument(
            lListA.size() == 2, "not a proper IPv4/mask string: " + aInTestNetworkAndSIDRA );
        Preconditions.checkArgument(
            lListB.size() == 2, "not a proper IPv4/mask string: " + aInTestNetworkAndSIDRB );

        long aInNetworkA = getNumberFromIPv4DotNotation( lListA.get( 0 ) );
        int aInNetworkLengthA = Integer.parseInt( lListA.get( 1 ) );
        long lNetworkMaskA = getIPv4NetMaskNumber( aInNetworkLengthA );

        long aInNetworkB = getNumberFromIPv4DotNotation( lListB.get( 0 ) );
        int aInNetworkLengthB = Integer.parseInt( lListB.get( 1 ) );
        long lNetworkMaskB = getIPv4NetMaskNumber( aInNetworkLengthB );

        aInNetworkA  = aInNetworkA & lNetworkMaskA;
        aInNetworkB  = aInNetworkB & lNetworkMaskB;

        return isIPv4AndLengthAASubnetOfB(
            aInNetworkA,
            getIPv4NetMaskNumber( aInNetworkLengthA ),
            aInNetworkB,
            getIPv4NetMaskNumber( aInNetworkLengthB ) );
    }

    /**
     *
     * @param aInNetworkA           ie: 4294967040
     * @param aInNetworkMaskA       ie: 16909056
     * @param aInNetworkB           same thing
     * @param aInNetworkMaskB       same thing
     * @return
     */
    private static boolean isIPv4AndLengthAASubnetOfB(
        long aInNetworkA,
        long aInNetworkMaskA,
        long aInNetworkB,
        long aInNetworkMaskB)
    {
        boolean lRet;

        int lNetworkLengthA = getIPv4NetMaskLength( aInNetworkMaskA );
        int lNetworkLengthB = getIPv4NetMaskLength( aInNetworkMaskB );

        if ( lNetworkLengthA >= lNetworkLengthB )
        {
            long lAndA = aInNetworkA & aInNetworkMaskB;  // yes.. no bug is A with B...
            long lAndB = aInNetworkB & aInNetworkMaskB;

            lRet = ( lAndA == lAndB );
        }
        else
        {
            lRet = false;
        }
        
        return lRet;
    }

    /**
     *
     * @param aInDotNotation  "1.2.3.4"
     * @return                number like this; 16845313
     */
    public static long getNumberFromIPv4DotNotation ( String aInDotNotation )
    {
        Preconditions.checkArgument(
            isPossiblyAnIpv4Address( aInDotNotation ),
            "invalid IPv4 format, aInDotNotation:" + aInDotNotation );

        final List<String> lOctetStrings = StringUtil.splitOverString( aInDotNotation, "." );

        int lOctetPosition = 0;
        long lReturnValue = 0;
        final long lByteMultipliers[] = { 256L*256L*256L, 256L*256L, 256L, 1L };

        for ( String lOctetAsString : lOctetStrings )
        {
            lReturnValue = lReturnValue + lByteMultipliers[lOctetPosition] * Long.parseLong( lOctetAsString );
            lOctetPosition++;
        }

        return lReturnValue;
    }

    /**
     *
     * @param aInIPv4AddressAsNumber    ie: 16777216
     * @return                          ie: "1.0.0.0"
     */
    public static String getIPv4DotNotationFromNumber ( long aInIPv4AddressAsNumber )
    {
        long lResult[] = new long[4];

        long lNext = aInIPv4AddressAsNumber;
        for ( int i=3; i>=0; i-- )
        {
            lResult[i] = lNext % 256;
            lNext = lNext / 256;
        }

        return lResult[0] + "." + lResult[1] + "." + lResult[2] + "." + lResult[3];
    }

    public static String getIPv4NetMaskLength(String aInMaskAsString)
    {
        return Integer.toString( getIPv4NetMaskLength( getNumberFromIPv4DotNotation( aInMaskAsString ) ) );
    }

    /**
     * 
     * @param aInMaskAsANumber     ie: 2147483648
     * @return                     ie: 31
     */
    public static int getIPv4NetMaskLength( long aInMaskAsANumber )
    {
        int lFirstPositionHasAOne = -1;

        for ( int i=0; i<32; i++ )
        {
            long lMasked = ( 1L << i ) & aInMaskAsANumber;
            if ( lMasked != 0 )
            {
                lFirstPositionHasAOne = i;
                break;
            }
        }

        int lRet;

        if ( lFirstPositionHasAOne == -1 )
        {
            // no ones, so all zeros, therefore length = 0;
            lRet = 0;
        }
        else
        {
            if ( lFirstPositionHasAOne == 31)
            {
                lRet = 1;
            }
            else
            {
                for ( int i=lFirstPositionHasAOne + 1; i<32; i++ )
                {
                    long lMasked = ( 1L << i ) & aInMaskAsANumber;
                    Preconditions.checkArgument(
                        lMasked != 0,
                        "first one is at bit position: " + lFirstPositionHasAOne +
                        ", but found a zero at higher position: " + i +
                        ", not a valid IPv4 network mask");
                }

                lRet = 32 - lFirstPositionHasAOne;
            }
        }

        return lRet;
    }

    public static String removeInterfaceIdFromAnIpv6Address( String aInIPv6Address )
    {
        Preconditions.checkArgument( IPAddressUtil.isPossiblyAnIpv6Address( aInIPv6Address) );

        String lRet;

        int lIndexOfPercentage = aInIPv6Address.indexOf( "%" );

        if ( lIndexOfPercentage != -1 )
        {
            lRet = aInIPv6Address.substring( 0, lIndexOfPercentage );
        }
        else
        {
            lRet = aInIPv6Address;
        }

        return lRet;
    }

    /**
     *  see junit test TestIPAddressUtil.testIPv6AddressFormat()
     *
     * @param     aInAddressAndLength   ie: either 1::3/45 or 1::3
     * @return
     */
    public static boolean isPossiblyAnIpv6AddressAndLength( String aInAddressAndLength )
    {
        String lAddress;
        int lLength;

        if ( hasAddressAndLengthIPv4AndIPv6( aInAddressAndLength ) )
        {
            try
            {
                lLength = Integer.parseInt( getLengthFromAddressAndLengthIPv4AndIPv6( aInAddressAndLength ) );
                lAddress = getAddressFromAddressAndLengthIPv4AndIPv6( aInAddressAndLength );
            }
            catch ( NumberFormatException ex )
            {
                lLength = -1;
                lAddress = null;
            }
        }
        else
        {
            lLength = 128;
            lAddress = aInAddressAndLength;
        }

        return
            lLength > 0 &&
                lLength <= 128 &&
                lAddress != null &&
                textIPv6AddressToArrayFormat( lAddress ) != null;
    }

    /**
     *  see junit test TestIPAddressUtil.testIPv4AddressFormat()
     *
     * @param     aInAddressAndLength
     * @return
     */
    public static boolean isPossiblyAnIpv4AddressAndLength( String aInAddressAndLength )
    {
        String lAddress;
        int lLength;

        if ( hasAddressAndLengthIPv4AndIPv6( aInAddressAndLength ) )
        {
            try
            {
                lLength = Integer.parseInt( getLengthFromAddressAndLengthIPv4AndIPv6( aInAddressAndLength ) );
                lAddress = getAddressFromAddressAndLengthIPv4AndIPv6( aInAddressAndLength );
            }
            catch ( NumberFormatException ex )
            {
                lLength = -1;
                lAddress = null;
            }
        }
        else
        {
            lLength = 32;
            lAddress = aInAddressAndLength;
        }

        return
            lLength > 0 &&
                lLength <= 32 &&
                lAddress != null &&
                textIPv4AddressToArrayFormat( lAddress ) != null;
    }

    /**
     *  see junit test TestIPAddressUtil.testIPv6AddressFormat()
     *
     * @param     aInAddress ie: 1::3
     * @return
     */
    public static boolean isPossiblyAnIpv6Address( String aInAddress )
    {
        return textIPv6AddressToArrayFormat( aInAddress ) != null;
    }

    /**
     *
     * @param aInTest    1.2.3.4
     * @return
     */
    public static boolean isPossiblyAnIpv4Address( String aInTest )
    {
        boolean lRet;

        if ( aInTest.contains ( ".." ) || aInTest.endsWith ( "." ) )
        {
            lRet = false;
        }
        else
        {
            final List<String> lOctetList = StringUtil.splitOverString ( aInTest, "." );

            if ( lOctetList.size () != 4 )
            {
                lRet = false;
            }
            else
            {
                lRet = true;

                for ( String lNext : lOctetList )
                {
                    try
                    {
                        int lNumber = Integer.parseInt ( lNext );

                        if ( lNumber < 0 || lNumber > 255 )
                        {
                            lRet = false;
                            break;
                        }
                    }

                    catch ( NumberFormatException ex )
                    {
                        lRet = false;
                        break;
                    }
                }
            }
        }

        return lRet;
    }
   
    /**
     * Compares two IPv4 addresses.
     * @param aInIPv4Addr1 the first address in dotted decimal format.
     * @param aInIPv4Addr2 the second address in dotted decimal format.
     * @return 0 if they are the same; 
     *         positive int if aInIPv4Addr1 > aInIPv4Addr2; 
     *         negative int if aInIPv4Addr1 < aInIPv4Addr2
     * @throws IllegalArgumentException if either of the IP addresses passed in is in the incorrect format.
     */
    public static int compareIPv4Addresses( String aInIPv4Addr1, String aInIPv4Addr2 )
    {
        byte[] lIPv4Addr1 = textIPv4AddressToArrayFormat( aInIPv4Addr1 );
        
        byte[] lIPv4Addr2 = textIPv4AddressToArrayFormat( aInIPv4Addr2 );
        
        return compareIPv4OrIPv6AddressesInArrayForm( InetAddrType.ipv4, lIPv4Addr1, lIPv4Addr2 );
    }

    private static String convertATwoBytesChunkIntoString(
        byte aInHighByte,
        byte aInLowByte,
        boolean aInExpandWithZerosInsideChunk)
    {
        int high = ( (int) aInHighByte ) & 0xff;
        int low = ( (int) aInLowByte ) & 0xff;
        int total = high * 256 + low;

        String lRet;

        if( aInExpandWithZerosInsideChunk )
        {
            lRet = String.format( "%04x", total );
        }
        else
        {
            lRet = Integer.toHexString( total );
        }

        return lRet;
    }

    /**
     * expands an IPv6 addresses.
     * @param aInIPv6Addr in a compressed or uncompressed format, like either 1::2 or 1:0:0:0:0:0:0:2
     * @param aInExpandZerosInside16BitChunks will make ie: the number 'ac' to be either 'ac' or '00ac' inside each 16 bits portion
     * @return the same address but in an uncompressed format
     * @throws IllegalArgumentException if the input argument is invalid, isPoss
     */
    public static String expandIPv6Address(
        String aInIPv6Addr,
        boolean aInExpandZerosInside16BitChunks )
    {
        byte[] lIPv6Addr = textIPv6AddressToArrayFormat( aInIPv6Addr );
        
        Preconditions.checkArgument( null != lIPv6Addr, "invalid address, value is: [" + aInIPv6Addr + "]" );

        StringBuffer lSb = new StringBuffer( );

        for ( int i=0; i<8; i++ )
        {
            lSb.append(
                convertATwoBytesChunkIntoString(
                    lIPv6Addr[ 2 * i ],
                    lIPv6Addr[ 2 * i + 1 ],
                    aInExpandZerosInside16BitChunks )
            );

            if( i < 7)
            {
                lSb.append(":");
            }
        }

        return lSb.toString();
    }

    /**
     * Compares two IPv6 addresses.
     * @param aInIPv6Addr1 the first address in dotted decimal format.
     * @param aInIPv6Addr2 the second address in dotted decimal format.
     * @return 0 if they are the same; 
     *         positive int if aInIPv6Addr1 > aInIPv6Addr2; 
     *         negative int if aInIPv6Addr1 < aInIPv6Addr2
     * @throws IllegalArgumentException if either of the IP addresses passed in is in the incorrect format.
     */
    public static int compareIPv6Addresses( String aInIPv6Addr1, String aInIPv6Addr2 )
    {
        byte[] lIPv6Addr1 = textIPv6AddressToArrayFormat( aInIPv6Addr1 );
        
        Preconditions.checkArgument( null != lIPv6Addr1, "invalid address, value is: [" + aInIPv6Addr1 + "]" );
        
        byte[] lIPv6Addr2 = textIPv6AddressToArrayFormat( aInIPv6Addr2 );
        
        Preconditions.checkArgument( null != lIPv6Addr1, "invalid address, value is: [" + aInIPv6Addr2 + "]" );

        return compareIPv4OrIPv6AddressesInArrayForm( InetAddrType.ipv6, lIPv6Addr1, lIPv6Addr2 );
    }

    public static byte[] getNumberFromIPv6DotNotation ( String aInIPv6Addr )
    {
        return textIPv6AddressToArrayFormat( aInIPv6Addr );
    }

    private static int compareIPv4OrIPv6AddressesInArrayForm(
        InetAddrType aInIpAddrType,
        byte[] aInIPAddr1,
        byte[] aInIPAddr2)
    {
        int lResult = 0;
        int lLength = Math.min(aInIPAddr1.length, aInIPAddr2.length);

        // Iterate through all the octets of the IP addresses.
        for (int i = 0; i < lLength; i++)
        {
            // convert the signed bytes to unsigned int(s) ...
            int lOctet1 = (aInIpAddrType == InetAddrType.ipv4) ? aInIPAddr1[i] & 0xff : aInIPAddr1[i] &  0xffff;
            int lOctet2 = (aInIpAddrType == InetAddrType.ipv4) ? aInIPAddr2[i] & 0xff : aInIPAddr2[i] &  0xffff;

            // and compare till we see the first difference, if any.
            if (lOctet1 != lOctet2)
            {
                lResult = lOctet1 - lOctet2;
                break;
            }
        }

        return lResult;
    }

    private final static int INADDR4SZ = 4;
    private final static int INADDR16SZ = 16;
    private final static int INT16SZ = 2;
    
    /*
     * Converts IPv4 address from its textual presentation form into its numeric binary form.
     *
     * @param    a String representing an IPv4 address in standard format
     * @return   a byte array representing the IPv4 numeric address
     */
    public static byte[] textIPv4AddressToArrayFormat( String aInIPAddress )
    {
        if (aInIPAddress.length() == 0) {
            return null;
        }

        byte[] res = new byte[INADDR4SZ];
        String[] s = aInIPAddress.split("\\.", -1);
        long val;
        try {
            switch(s.length) {
                case 1:
                    /*
                    * When only one part is given, the value is stored directly in
                    * the network address without any byte rearrangement.
                    */

                    val = Long.parseLong(s[0]);
                    if (val < 0 || val > 0xffffffffL)
                        return null;
                    res[0] = (byte) ((val >> 24) & 0xff);
                    res[1] = (byte) (((val & 0xffffff) >> 16) & 0xff);
                    res[2] = (byte) (((val & 0xffff) >> 8) & 0xff);
                    res[3] = (byte) (val & 0xff);
                    break;
                case 2:
                    /*
                    * When a two part address is supplied, the last part is
                    * interpreted as a 24-bit quantity and placed in the right
                    * most three bytes of the network address. This makes the
                    * two part address format convenient for specifying Class A
                    * network addresses as net.host.
                    */

                    val = Integer.parseInt(s[0]);
                    if (val < 0 || val > 0xff)
                        return null;
                    res[0] = (byte) (val & 0xff);
                    val = Integer.parseInt(s[1]);
                    if (val < 0 || val > 0xffffff)
                        return null;
                    res[1] = (byte) ((val >> 16) & 0xff);
                    res[2] = (byte) (((val & 0xffff) >> 8) &0xff);
                    res[3] = (byte) (val & 0xff);
                    break;
                case 3:
                    /*
                    * When a three part address is specified, the last part is
                    * interpreted as a 16-bit quantity and placed in the right
                    * most two bytes of the network address. This makes the
                    * three part address format convenient for specifying
                    * Class B net- work addresses as 128.net.host.
                    */
                    for (int i = 0; i < 2; i++) {
                        val = Integer.parseInt(s[i]);
                        if (val < 0 || val > 0xff)
                            return null;
                        res[i] = (byte) (val & 0xff);
                    }
                    val = Integer.parseInt(s[2]);
                    if (val < 0 || val > 0xffff)
                        return null;
                    res[2] = (byte) ((val >> 8) & 0xff);
                    res[3] = (byte) (val & 0xff);
                    break;
                case 4:
                    /*
                    * When four parts are specified, each is interpreted as a
                    * byte of data and assigned, from left to right, to the
                    * four bytes of an IPv4 address.
                    */
                    for (int i = 0; i < 4; i++) {
                        val = Integer.parseInt(s[i]);
                        if (val < 0 || val > 0xff)
                            return null;
                        res[i] = (byte) (val & 0xff);
                    }
                    break;
                default:
                    return null;
            }
        } catch(NumberFormatException e) {
            return null;
        }
        return res;
    }
    
    /*
    *   Convert IPv6 presentation level address to network order binary form.
    *   credit:
    *     Converted from C code from Solaris 8 (inet_pton)
    *
    *   Any component of the string following a per-cent % is ignored.
    *
    *   @param     a String representing an IPv6 address in textual format
    *   @return    a byte array representing the IPv6 numeric address
    */
    private static byte[] textIPv6AddressToArrayFormat( String aInIPAddress )
    {
        // Shortest valid string is "::", hence at least 2 chars
        if ( aInIPAddress == null || aInIPAddress.length() < 2) {
            return null;
        }

        int colonp;
        char ch;
        boolean saw_xdigit;
        int val;
        char[] srcb = aInIPAddress.toCharArray();
        byte[] dst = new byte[INADDR16SZ];

        int srcb_length = srcb.length;
        int pc = aInIPAddress.indexOf ("%");
        if (pc == srcb_length -1) {
            return null;
        }

        if (pc != -1) {
            srcb_length = pc;
        }

        colonp = -1;
        int i = 0, j = 0;
        /* Leading :: requires some special handling. */
        if (srcb[i] == ':')
            if (srcb[++i] != ':')
                return null;
        int curtok = i;
        saw_xdigit = false;
        val = 0;
        while (i < srcb_length) {
            ch = srcb[i++];
            int chval = Character.digit(ch, 16);
            if (chval != -1) {
                val <<= 4;
                val |= chval;
                if (val > 0xffff)
                    return null;
                saw_xdigit = true;
                continue;
            }
            if (ch == ':') {
                curtok = i;
                if (!saw_xdigit) {
                    if (colonp != -1)
                        return null;
                    colonp = j;
                    continue;
                } else if (i == srcb_length) {
                    return null;
                }
                if (j + INT16SZ > INADDR16SZ)
                    return null;
                dst[j++] = (byte) ((val >> 8) & 0xff);
                dst[j++] = (byte) (val & 0xff);
                saw_xdigit = false;
                val = 0;
                continue;
            }
            if (ch == '.' && ((j + INADDR4SZ) <= INADDR16SZ)) {
                String ia4 = aInIPAddress.substring(curtok, srcb_length);
                /* check this IPv4 address has 3 dots, ie. A.B.C.D */
                int dot_count = 0, index=0;
                while ((index = ia4.indexOf ('.', index)) != -1) {
                    dot_count ++;
                    index ++;
                }
                if (dot_count != 3) {
                    return null;
                }
                byte[] v4addr = textIPv4AddressToArrayFormat( ia4 );
                if (v4addr == null) {
                    return null;
                }
                for (int k = 0; k < INADDR4SZ; k++) {
                    dst[j++] = v4addr[k];
                }
                saw_xdigit = false;
                break;  /* '\0' was seen by inet_pton4(). */
            }
            return null;
        }
        if (saw_xdigit) {
            if (j + INT16SZ > INADDR16SZ)
                return null;
            dst[j++] = (byte) ((val >> 8) & 0xff);
            dst[j++] = (byte) (val & 0xff);
        }

        if (colonp != -1) {
            int n = j - colonp;

            if (j == INADDR16SZ)
                return null;
            for (i = 1; i <= n; i++) {
                dst[INADDR16SZ - i] = dst[colonp + n - i];
                dst[colonp + n - i] = 0;
            }
            j = INADDR16SZ;
        }
        if (j != INADDR16SZ)
            return null;
        /*
        byte[] newdst = convertFromIPv4MappedAddressInArrayFormat( dst );
        if (newdst != null) {
            return newdst;
        } else {
            return dst;
        } */
        return dst;
    }

    /*
    * Convert IPv4-Mapped address to IPv4 address. Both input and
    * returned value are in network order binary form.
    *
    * @param src a String representing an IPv4-Mapped address in textual format
    * @return a byte array representing the IPv4 numeric address
    */
    private static byte[] convertFromIPv4MappedAddressInArrayFormat( byte[] addr ) 
    {
        if ( isIPv4MappedIPv6Address( addr )) {
            byte[] newAddr = new byte[INADDR4SZ];
            System.arraycopy(addr, 12, newAddr, 0, INADDR4SZ);
            return newAddr;
        }
        return null;
    }
    
    /**
     * Utility routine to check if the InetAddress is an
     * IPv4 mapped IPv6 address.
     *
     * @return a <code>boolean</code> indicating if the InetAddress is
     * an IPv4 mapped IPv6 address; or false if address is IPv4 address.
     */
    private static boolean isIPv4MappedIPv6Address( byte[] addr ) 
    {
        boolean lRet = false;
        
        if (addr.length == INADDR16SZ) 
        {
            if ((addr[0] == 0x00) && (addr[1] == 0x00) &&
                (addr[2] == 0x00) && (addr[3] == 0x00) &&
                (addr[4] == 0x00) && (addr[5] == 0x00) &&
                (addr[6] == 0x00) && (addr[7] == 0x00) &&
                (addr[8] == 0x00) && (addr[9] == 0x00) &&
                (addr[10] == (byte)0xff) &&
                (addr[11] == (byte)0xff))  
            {
                lRet = true;
            }
        }
        
        return lRet;
    }

    public static List<String> getLocalHostIPAddresses( IFilter aInIFilter ) throws SocketException
    {
        Enumeration<NetworkInterface> lNetItfs = NetworkInterface.getNetworkInterfaces();
        List<String> lAllIps = new ArrayList<String>();
        while ( lNetItfs.hasMoreElements() )
        {
            NetworkInterface lItf = lNetItfs.nextElement();
            for ( InterfaceAddress lInterfaceAddress : lItf.getInterfaceAddresses() )
            {
                InetAddress lIpAddr = lInterfaceAddress.getAddress();
                if ( aInIFilter.doesPass( lIpAddr ) )
                {
                    lAllIps.add( lIpAddr.getHostAddress() );
                }
            }
        }
        return lAllIps;
    }

    public static boolean isMappedIPv4OnIPv6Address( String aInMappedIPv6 )
    {
        byte[] lArrayFormat = textIPv6AddressToArrayFormat( aInMappedIPv6 );
        
        return ( lArrayFormat != null ) && isIPv4MappedIPv6Address( lArrayFormat );
    }

    public static String getIPV4PartFromMappedIPv4OnIPv6Address( String aInMappedIPv6 )
    {
        Preconditions.checkArgument( isMappedIPv4OnIPv6Address( aInMappedIPv6 ) );

        byte[] lArrayFormat = textIPv6AddressToArrayFormat( aInMappedIPv6 );

        return 
            Integer.toString( lArrayFormat[12] ) + "." + 
            Integer.toString( lArrayFormat[13] ) + "." + 
            Integer.toString( lArrayFormat[14] ) + "." + 
            Integer.toString( lArrayFormat[15] ); 
    }
    
    public static boolean isZeroIPv6Address( String aInTestIP )
    {
        byte[] lArrayFormat = textIPv6AddressToArrayFormat( aInTestIP );
        
        return 
            ( lArrayFormat != null ) && 
            lArrayFormat[0] == 0 && lArrayFormat[1] == 0 && lArrayFormat[2] == 0 && lArrayFormat[3] == 0 &&
            lArrayFormat[4] == 0 && lArrayFormat[5] == 0 && lArrayFormat[6] == 0 && lArrayFormat[7] == 0 &&
            lArrayFormat[8] == 0 && lArrayFormat[9] == 0 && lArrayFormat[10] == 0 && lArrayFormat[11] == 0 && 
            lArrayFormat[12] == 0 &&  lArrayFormat[13] == 0 && lArrayFormat[14] == 0 && lArrayFormat[15] == 0;
    }

    public static boolean isIPv4LoopbackAddress( String aInTestIP )
    {
        byte[] lArrayFormat = textIPv4AddressToArrayFormat( aInTestIP );
        
        return 
            ( lArrayFormat != null ) && 
            lArrayFormat[0] == (byte)0x7f && lArrayFormat[1] == 0 && lArrayFormat[2] == 0 && lArrayFormat[3] == 1;
    }
    
    public static boolean isIPv6LoopbackAddress( String aInTestIP )
    {
        byte[] lArrayFormat = textIPv6AddressToArrayFormat( aInTestIP );
        
        return 
            ( lArrayFormat != null ) && 
            lArrayFormat[0] == 0 && lArrayFormat[1] == 0 && lArrayFormat[2] == 0 && lArrayFormat[3] == 0 &&
            lArrayFormat[4] == 0 && lArrayFormat[5] == 0 && lArrayFormat[6] == 0 && lArrayFormat[7] == 0 &&
            lArrayFormat[8] == 0 && lArrayFormat[9] == 0 && lArrayFormat[10] == 0 && lArrayFormat[11] == 0 && 
            lArrayFormat[12] == 0 &&  lArrayFormat[13] == 0 && lArrayFormat[14] == 0 && lArrayFormat[15] == 1;
    }

    public static boolean isIPv6LinkLocalAddress( String aInTestIP )
    {
        byte[] lArrayFormat = textIPv6AddressToArrayFormat( aInTestIP );

        return
            ( lArrayFormat != null ) &&
                lArrayFormat[0] == (byte)0xfe && lArrayFormat[1] == (byte)0x80 && lArrayFormat[2] == 0 && lArrayFormat[3] == 0 &&
                lArrayFormat[4] == 0          && lArrayFormat[5] == 0          && lArrayFormat[6] == 0 && lArrayFormat[7] == 0;
    }

    public static boolean isNullOrEmpty ( String aInIpAddress )
    {
        return ( null == aInIpAddress || aInIpAddress.isEmpty () );
    }
}

