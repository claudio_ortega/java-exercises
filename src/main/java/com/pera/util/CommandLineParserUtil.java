/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2019.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import joptsimple.OptionDescriptor;
import joptsimple.internal.Strings;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CommandLineParserUtil
{
    private CommandLineParserUtil() {}

    public static class HelpFormatter implements joptsimple.HelpFormatter
    {
        public String format( Map<String, ? extends OptionDescriptor> options )
        {
            final int[] COL_WIDTHS = {20,23,40};
            final StringBuilder sb = new StringBuilder();

            final String LINE_FORMAT =
                "%-"+ COL_WIDTHS[0] + "s" +
                "%-"+ COL_WIDTHS[1] + "s" +
                "%-"+ COL_WIDTHS[2] + "s" + "%n";

            sb.append( String.format( LINE_FORMAT, "option name", "default value", "description") );
            sb.append( Strings.repeat('-', COL_WIDTHS[0]+COL_WIDTHS[1]+COL_WIDTHS[2]) + "\n" );

            final List<String> orderedKeys = new LinkedList<>(options.keySet());
            orderedKeys.sort( Comparator.naturalOrder() );

            for ( final String nextKey : orderedKeys )
            {
                final OptionDescriptor descriptor = options.get( nextKey);

                if ( !descriptor.representsNonOptions() )
                {
                    final List<String> limitedLines = StringUtil.getLimitedLines( descriptor.description(), COL_WIDTHS[2] );

                    for ( int i=0; i< limitedLines.size(); i++ )
                    {
                        final String line = limitedLines.get(i);

                        if ( i == 0 )
                        {
                            sb.append(String.format(
                                LINE_FORMAT,
                                descriptor.options().get(0),
                                getDefaultExp(descriptor),
                                line)
                            );
                        }
                        else
                        {
                            sb.append(String.format(
                                LINE_FORMAT,
                                "",
                                "",
                                line)
                            );
                        }
                    }
                }

                sb.append("\n");
            }

            return sb.toString();
        }

        private static String getDefaultExp( OptionDescriptor descriptor )
        {
            final String ret;

            if ( ! descriptor.acceptsArguments() )
            {
                ret = "no arguments";
            }
            else
            {
                if ( descriptor.defaultValues().isEmpty() )
                {
                    ret = "no default value";
                }
                else
                {
                    ret = descriptor.defaultValues().toString();
                }
            }

            return ret;
        }
    }
}

