
/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import org.apache.commons.lang3.time.*;
import java.util.*;

public class TimeUtil
{
    private TimeUtil()
    {
    }

    public static String formatDuration( long millis )
    {
        return DurationFormatUtils.formatPeriod(
            0,
            millis,
            "d'd' HH:mm",
            true,
            TimeZone.getDefault() );
    }
}
