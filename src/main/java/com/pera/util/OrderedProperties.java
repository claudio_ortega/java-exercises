/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import org.apache.logging.log4j.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.atomic.*;

public class OrderedProperties
{
    private final Logger logger = LogManager.getLogger();

    private List<String> keys;
    private Map<String,String> kvPairs;

    public static OrderedProperties create()
    {
        return new OrderedProperties();
    }

    private OrderedProperties()
    {
        keys = new LinkedList<>();
        kvPairs = new HashMap<>();
    }

    public Map<String,String> getMap()
    {
        return kvPairs;
    }

    /**
     * @param      map used modify existing values, addition or removal is not supported
     * @return
     */
    public OrderedProperties combine ( Map<String,String> map )
    {
        map.forEach( (k,v) -> kvPairs.put(k,v) );
        return this;
    }

    public void toFile (File file ) throws Exception
    {
        try ( final PrintStream out = FileUtil.createPrintStreamFromFile( file, false ) ) {
            out.println( "#");
            out.println( "# config file saved from app" );
            out.println( "# " + new SimpleDateFormat( "yyyy-MM-dd.HH:mm:ss.SSSZZZ" ).format(new Date()) );
            out.println( "#");
            out.println( "#");
            keys.forEach( k -> out.println( String.format( "%s=%s", k, kvPairs.get( k ) ) ) );
        }
    }

    public OrderedProperties fromFile (File file ) throws IOException
    {
        keys.clear();
        kvPairs.clear();

        final AtomicLong lineCounter = new AtomicLong(0);
        final List<String> lines = FileUtil.getLinesFromFile( file );

        lines.forEach(
            l -> {
                lineCounter.incrementAndGet();
                final String line = l.trim();

                if ( ! line.startsWith( "#" ) && ! line.isEmpty() )
                {
                    if ( !line.contains("=") )
                    {
                        logger.warn("ignoring line {}, content: [{}]", lineCounter.get(), l);
                    }
                    else
                    {
                        final List<String> split = Arrays.asList(line.split("="));

                        if ( split.size() == 2 )
                        {
                            keys.add(split.get(0).trim());
                            kvPairs.put(split.get(0).trim(), split.get(1).trim());
                        }
                        else if ( split.size() == 1 )
                        {
                            keys.add(split.get(0).trim());
                            kvPairs.put(split.get(0).trim(), "");
                        }
                        else
                        {
                            logger.warn("ignoring line {}, content: [{}]", lineCounter.get(), l);
                        }
                    }
                }
            }
        );

        return this;
    }
}
