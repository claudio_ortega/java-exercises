/*
 * Copyright (c) Claudio Ortega and Pera Labs - 2018.
 * Written by Claudio Ortega.
 * All rights reserved in all countries.
 *
 */

package com.pera.util;

import com.google.common.base.Preconditions;
import org.apache.logging.log4j.*;
import sun.tools.jconsole.LocalVirtualMachine;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class SystemUtil
{
    private final static Logger logger = LogManager.getLogger();

    // you shall not instantiate this class
    private SystemUtil ()
    {
    }

    public static boolean ensureWeHaveAnIPAddressForService(int tries )
    {
        boolean found = false;

        for ( int i=1; i<=tries; i++ )
        {
            final String validIP = getAValidLocalIP( "", false );

            if ( validIP.isEmpty() )
            {
                logger.info( "did not find an available IP at try {}/{}, will sleep for one second.. ",
                    i,
                    tries );
                sleepMsec( 1_000 );
            }
            else
            {
                found = true;
                logger.info( "found an available IP: {} at try {}/{}", validIP, i, tries );
                break;
            }
        }

        if ( ! found )
        {
            logger.info("did not find any available IP after {} tries", tries);
        }

        return found;
    }

    public static class Constants
    {
        public static final String LINE_SEPARATOR = System.getProperty( "line.separator" );
        public static final String PATH_SEPARATOR = System.getProperty( "path.separator" );
        public static final String FILE_SEPARATOR = System.getProperty( "file.separator" );
        public static final String USER_HOME_DIR = System.getProperty( "user.home" );
        public static final String USER_NAME = System.getProperty( "user.name" );
    }

    public static List<String> getJavaExtraArguments ()
    {
        return ManagementFactory.getRuntimeMXBean().getInputArguments ();
    }

    public static File getCurrentDirectory ()
    {
        return FileUtil.getFileWithAbsolutePath ( new File ( "." ) );
    }

    public static void sleepMsec( long aInMsec )
    {
        try
        {
            Thread.sleep ( aInMsec );
        }
        catch ( Exception ex )
        {
            // do nothing
        }
    }

    public static String getPIDForThisProcess ( String aInDefaultIfNotFound )
    {
        final String lThisProcessName = ManagementFactory.getRuntimeMXBean ().getName ();
        final int lIndexOfAt = lThisProcessName.indexOf ( "@" );

        final String lReturn;

        if ( lIndexOfAt >= 0 )
        {
            lReturn = lThisProcessName.substring ( 0, lIndexOfAt );
        }
        else
        {
            lReturn = aInDefaultIfNotFound;
        }

        return lReturn;
    }

    public static String getEnvironmentOrSysProperty ( String name )
    {
        String ret = System.getenv ( name );

        if ( ret == null )
        {
            ret = System.getProperty ( name );
        }

        return ret;
    }

    public static SimpleDateFormat getUTCHighPrecisionDateFormat()
    {
        final SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat ( "yyyy-MM-dd.HH:mm:ss.SSSZZZ" );
        lSimpleDateFormat.setTimeZone ( TimeZone.getTimeZone ( "UTC" ) );
        return lSimpleDateFormat;
    }

    public static SimpleDateFormat getHighPrecisionDateFormat()
    {
        return new SimpleDateFormat ( "yyyy-MM-dd.HH:mm:ss.SSSZZZ" );
    }

    public enum OSType { Windows, Mac, Linux }

    public static OSType getOSType()
    {
        final String lOSName = System.getProperty ( "os.name" ).toLowerCase ( );

        final OSType lRet;

        if ( lOSName.contains ( "win" ) )
        {
            lRet = OSType.Windows;
        }

        else if ( lOSName.contains ( "mac" ) )
        {
            lRet = OSType.Mac;

        }

        else if ( lOSName.contains ( "inux" ) )
        {
            lRet = OSType.Linux;
        }

        else
        {
            lRet = null;
        }

        Preconditions.checkState ( lRet != null, "lOSName:" + lOSName );

        return lRet;
    }

    public static Set<Integer> getJavaVMsMatching(String aInLookupMainClassName )
    {
        final Set<Integer> matchingPids = new HashSet<>();

        for ( final Map.Entry<Integer, LocalVirtualMachine> entry : LocalVirtualMachine.getAllVirtualMachines().entrySet() )
        {
            final String line = entry.getValue().displayName ();

            if ( aInLookupMainClassName == null || line.contains ( aInLookupMainClassName ) )
            {
                matchingPids.add( entry.getKey () );
                logger.debug ( "found lookup value [{}] with PID: [{}], line:[{}]",
                    aInLookupMainClassName,
                    entry.getKey (),
                    line );
            }
        }

        return matchingPids;
    }

    private static Set<String> getLocalIPAddresses(
        boolean aInIncludeIPv4,
        boolean aInIncludeIPv6,
        boolean aInIncludeIPv4OrIpv6LoopBacks,
        boolean aInIncludeIPv6LinkLocal)
    {
        Preconditions.checkArgument( aInIncludeIPv6 || ! aInIncludeIPv6LinkLocal, "include IPv6 local only supported when you include IPv6" );

        Set<String> lReturn = new HashSet<>();

        try
        {
            Enumeration<NetworkInterface> lInterfaceEnumeration = NetworkInterface.getNetworkInterfaces ();

            while ( lInterfaceEnumeration.hasMoreElements() )
            {
                NetworkInterface lNetworkInterface = lInterfaceEnumeration.nextElement();

                Enumeration<InetAddress> lInetAddressEnumeration = lNetworkInterface.getInetAddresses();

                while ( lInetAddressEnumeration.hasMoreElements() )
                {
                    InetAddress lNextIpAddr = lInetAddressEnumeration.nextElement();

                    String lNextIPAddressStringified = lNextIpAddr.getHostAddress();

                    if ( IPAddressUtil.isPossiblyAnIpv4Address( lNextIPAddressStringified ) )
                    {
                        if ( aInIncludeIPv4 )
                        {
                            if ( IPAddressUtil.isIPv4LoopbackAddress( lNextIPAddressStringified) )
                            {
                                if ( aInIncludeIPv4OrIpv6LoopBacks )
                                {
                                    lReturn.add( lNextIPAddressStringified );
                                }
                            }
                            else
                            {
                                lReturn.add( lNextIPAddressStringified );
                            }
                        }
                    }

                    else if ( IPAddressUtil.isPossiblyAnIpv6Address( lNextIPAddressStringified ) )
                    {
                        if ( aInIncludeIPv6 )
                        {
                            lNextIPAddressStringified = IPAddressUtil.expandIPv6Address(
                                IPAddressUtil.removeInterfaceIdFromAnIpv6Address( lNextIPAddressStringified ), false );

                            if ( IPAddressUtil.isIPv6LoopbackAddress( lNextIPAddressStringified) )
                            {
                                if ( aInIncludeIPv4OrIpv6LoopBacks )
                                {
                                    lReturn.add( lNextIPAddressStringified );
                                }
                            }

                            else if ( IPAddressUtil.isIPv6LinkLocalAddress( lNextIPAddressStringified) )
                            {
                                if ( aInIncludeIPv6LinkLocal )
                                {
                                    lReturn.add( lNextIPAddressStringified );
                                }
                            }

                            else
                            {
                                lReturn.add( lNextIPAddressStringified );
                            }

                        }
                    }

                    else
                    {
                        throw new IllegalArgumentException( "not a valid IP: [" + lNextIPAddressStringified + "]" );
                    }
                }
            }
        }

        catch ( SocketException ex )
        {
            logger.error( "unable to obtain IP addresses configured on this machine" );
            logger.debug ( ex.getMessage (), ex );
        }

        return lReturn;
    }

    public static String getAValidLocalIP ( String proposedIPOrHostName, boolean throwExeptionIfNoneAvailable )
    {
        String ret = "";

        if ( "localhost".equals( proposedIPOrHostName ) )
        {
            ret = proposedIPOrHostName;
        }

        else if ( proposedIPOrHostName.matches( "[a-zA-Z\\.\\-]+" ) )
        {
            // do no allow dns names
        }

        else if ( proposedIPOrHostName.length() == 0 )
        {
            // if passed empty, we assume the intention is for us to pick the
            // first available IP
            final Set<String> iPS = getLocalIPAddresses(
                true,
                false,
                false,
                false );

            if ( iPS.isEmpty( ) ) {
                logger.warn("there is no available IPs");
            } else {
                ret = iPS.iterator().next();
            }
        }

        else
        {
            // if an IP was passed, it has to be one of the available
            final Set<String> iPS = getLocalIPAddresses(
                true,
                false,
                true,
                false );

            if ( ! iPS.contains(proposedIPOrHostName) ) {
                logger.warn( "this IP: {} is not available", proposedIPOrHostName );
            } else {
                ret = proposedIPOrHostName;
            }
        }

        if ( ret.isEmpty() && throwExeptionIfNoneAvailable ) {
            throw new IllegalStateException( "there is no available IPs");
        }

        return ret;
    }

    /**
     * @param aInBootstrapClass         this class must exist in the same jar file from where we are going to extract the file that goes by aInSimpleFileName
     * @param aInResourceNameToExtract  the name of the file to be extract from the jar
     * @param aInOutputPathToCreate     the place in the FS to put the extracted file contents
     * @throws IOException
     */
    public static void extractFileFromJarFile (
        Class aInBootstrapClass,
        String aInResourceNameToExtract,
        File aInOutputPathToCreate )
            throws IOException
    {
        Preconditions.checkArgument ( ! aInOutputPathToCreate.exists ( ), "file :[" + aInOutputPathToCreate + "] should not exist" );

        final Set<URL> lFoundURLSet = listResourceFilesFromAllSources(
            aInBootstrapClass,
            aInResourceNameToExtract );

        logger.debug( "file [{}] was found in all these URLs:{}" + aInResourceNameToExtract, lFoundURLSet );

        Preconditions.checkState ( lFoundURLSet.size ( ) > 0, "file [" + aInResourceNameToExtract + "] should be included in the jars loadable from the class loader, but it was not found" );

        if ( lFoundURLSet.size() > 1 )
        {
            logger.warn(
                "file [" + aInResourceNameToExtract +
                    "] should be included in only one of the jars loadable from the class loader, " +
                    "but it was found in all these URLs: " + lFoundURLSet );
        }

        final InputStream lIS = lFoundURLSet.iterator().next().openStream();

        Preconditions.checkState ( lIS != null, "input stream is null" );

        final BufferedInputStream lBIS = new BufferedInputStream( lIS );

        final FileOutputStream lFOS = new FileOutputStream( aInOutputPathToCreate, false );
        final BufferedOutputStream lBOS = new BufferedOutputStream( lFOS );

        FileUtil.copyStream( lBIS, lBOS );

        lFOS.flush ();
        lFOS.close ();

        lIS.close ();

        Preconditions.checkState ( aInOutputPathToCreate.exists ( ), "file :[" + aInOutputPathToCreate + "] should exist" );
    }

    private static Set<URL> listResourceFilesFromAllSources (
        Class aInClass,
        String aInSimpleFileName )
        throws IOException
    {
        return CollectionsUtil.concatenateSets (
            getFileURLFromSystemClassLoader( aInSimpleFileName ),
            getFileURLFromClassLoader( aInClass.getClassLoader(), aInSimpleFileName ),
            getFileURLFromClassLoader( Thread.currentThread().getContextClassLoader(), aInSimpleFileName ) );
    }

    private static Set<URL> getFileURLFromSystemClassLoader( String aInSimpleFileName )
        throws IOException
    {
        final Set<URL> lFindings = new HashSet<>();

        final Enumeration<URL> lEnumeration = ClassLoader.getSystemResources ( aInSimpleFileName );

        while ( lEnumeration.hasMoreElements() )
        {
            final URL lNextURL = lEnumeration.nextElement();

            logger.debug( "using static getSystemResources(), found file [{}] inside [{}]",
                aInSimpleFileName,
                lNextURL.getFile() );

            lFindings.add ( lNextURL );
        }

        return lFindings;
    }

    private static Set<URL> getFileURLFromClassLoader(
        ClassLoader aInClassLoader,
        String aInSimpleFileName )
        throws IOException
    {
        final Set<URL> lFindings = new HashSet<> ();

        final Enumeration<URL> lEnumeration = aInClassLoader.getResources ( aInSimpleFileName );

        while ( lEnumeration.hasMoreElements() )
        {
            final URL lNextURL = lEnumeration.nextElement ();

            logger.debug( "using classloader: [{}, found file [{}] inside [{}]",
                aInClassLoader.getClass (),
                aInSimpleFileName,
                lNextURL.getFile() );

            lFindings.add ( lNextURL );
        }

        return lFindings;
    }

    public static String getGitVersion ( boolean aInIncludeHash )
    {
        return iGetGitVersion( aInIncludeHash, SystemUtil.class );
    }

    private static String iGetGitVersion ( boolean aInIncludeHash, Class bootstrapClass)
    {
        String lReturn;

        try
        {
            final String lGitDescribe = FileUtil.getContentFromResourceFile (
                bootstrapClass,
                "git-describe.txt",
                100 )
                .replace ( "\"", "" )
                .replace ( "\n", "" )
                .trim ( );

            if ( aInIncludeHash )
            {
                lReturn = lGitDescribe;
            }

            else
            {
                final int hyphenIndex = lGitDescribe.lastIndexOf ( "-" );

                if ( hyphenIndex == -1 )
                {
                    lReturn = lGitDescribe;
                }

                else
                {
                    lReturn = lGitDescribe.substring ( 0, lGitDescribe.lastIndexOf ( "-" ) );
                }
            }
        }

        catch ( Exception ex )
        {
            lReturn = "unable to determine git version";
            logger.error ( ex.getMessage (), ex );
        }

        return lReturn;
    }

    /**
     * Checks connectivity of host on specified port. It is checked by attempting to connect a client TCP socket
     * onto the specified ip address/port.
     * If the socket is successfully connected, it means then that such combination ip address/port has a server socket bound to it
     *
     * @param aInHost  host under test, either an ip address or a DNS resolvable name
     * @param aInPort  port under testTested port
     * @param aInTimeOutInMSecs timeout in milliseconds
     * @return true if and only if the client socket has been successfully connected to the specified ip address/port
     * @throws IOException
     */
    public static boolean canConnectOverTCP (
        String aInHost,
        int aInPort,
        int aInTimeOutInMSecs )
            throws IOException
    {
        boolean lRet;

        try ( final Socket lSocket = new Socket () )
        {
            lSocket.connect ( new InetSocketAddress ( aInHost, aInPort ), aInTimeOutInMSecs );
            lRet = lSocket.isConnected ();
        }
        catch ( ConnectException | NoRouteToHostException | SocketTimeoutException ex )
        {
            // not a problem, this means that we could not connect
            lRet = false;
        }

        return lRet;
    }

}