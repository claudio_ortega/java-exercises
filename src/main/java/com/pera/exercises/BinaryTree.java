package com.pera.exercises;

import java.util.HashSet;
import java.util.Set;

class BinaryTreeNode {

    interface Visitor {
        boolean visit( BinaryTreeNode btn );
    }

    private String id;
    private BinaryTreeNode left;
    private BinaryTreeNode right;
    private boolean isRoot;

    BinaryTreeNode( String _id, boolean _isRoot ) {
        id = _id;
        isRoot = _isRoot;
        left = null;
        right = null;
    }

    @Override
    public boolean equals(Object o) {
        BinaryTreeNode that = (BinaryTreeNode) o;
        return this.id.equals( that.id );
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return id;
    }

    boolean visitLeftOrder(Visitor visitor ) {
        if ( left != null ) {
            final boolean shouldStop = left.visitLeftOrder( visitor );
            if ( shouldStop ) {
                return true;
            }
        }

        {
            boolean shouldStop = visitor.visit(this);
            if (shouldStop) {
                return true;
            }
        }

        if ( right != null ) {
            final boolean shouldStop = right.visitLeftOrder( visitor );
            if ( shouldStop ) {
                return true;
            }
        }

        return false;
    }

    static class CycleChecker {

        private final Set<BinaryTreeNode> set;

        CycleChecker( ) {
            set = new HashSet<>();
        }

        boolean check( BinaryTreeNode start ) {
            return start.visitLeftOrder(
                ( node ) -> {
                    if ( set.contains( node ) ) {
                        System.out.printf("found repeated node:%s, set is:%s\n", node.id, set);
                        return true;
                    }
                    set.add( node );
                    return false;
                }
            );
        }
    }

    public static void main (String[] args) {

        final BinaryTreeNode r = new BinaryTreeNode( "root", true );

        final BinaryTreeNode a1 = new BinaryTreeNode( "a.1", true );
        final BinaryTreeNode a2 = new BinaryTreeNode( "a.2", true );

        r.left = a1;
        r.right = a2;

        final BinaryTreeNode a11 = new BinaryTreeNode( "a.1.1", true );
        final BinaryTreeNode a12 = new BinaryTreeNode( "a.1.2", true );

        a1.left = a11;
        a1.right = a12;

        //  create cycles
        // r.right = r;
        // a11.right = a12;

        final CycleChecker checker = new CycleChecker();

        if (checker.check( r )) {
            throw new RuntimeException("");
        }

        r.visitLeftOrder(
            ( node ) -> {
                final boolean stop = node.id.equals("a.1.2");   //("do-not-find-me");
                if ( stop ) {
                    System.out.printf("found node:%s\n", node.id);
                }
                return stop;
            }
        );

        System.out.println("passed");
    }
}

