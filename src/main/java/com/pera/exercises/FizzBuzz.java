package com.pera.exercises;

interface Consumer {
    void consume( int i );
}

class FizzBuzz {

    static void doIt( Consumer c ) {
        for ( int i=0; i<37; i++ ) {
            c.consume( i );
        }
    }

    public static void main (String[] args) {
        doIt(
            ( int i ) -> {
                if ( i % 5 == 0 && i % 3 == 0 ) {
                    System.out.printf("FizzBuzz at %d\n", i);
                    return;
                }
                if ( i % 3 == 0 ) {
                    System.out.printf("Fizz at %d\n", i);
                    return;
                }
                if ( i % 5 == 0 ) {
                    System.out.printf("Buzz at %d\n", i);
                }
            }
        );
    }
}
