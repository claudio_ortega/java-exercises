package com.pera.exercises;

class WaterContainer {

    public static void main (String[] args) {

        final int[] barriers = new int[]{1,8,6,2,5,4,8,3,7};
        // form pairs with left barrier lesser than right

        int maxLeft = 0;
        int maxRight = 0;
        int maxVolume = 0;

        for ( int left=0; left<barriers.length; left++) {
            for ( int right=left+1; right<barriers.length; right++) {
                final int volume = (right-left) * Math.min(barriers[right], barriers[left]);
                System.out.printf("left:%d, right:%d, volume:%d\n", left, right, volume);

                if ( volume > maxVolume ) {
                    maxLeft = left;
                    maxRight = right;
                    maxVolume = volume;
                }
            }
        }

        System.out.printf("maxLeft:%d, maxRight:%d, maxVolume:%d\n", maxLeft, maxRight, maxVolume);
    }
}

