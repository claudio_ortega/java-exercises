package com.pera.exercises;

/*
    Grail.sum(a,b) takes two strings with the binary representation of a,b
    and returns a string with the binary representation of a+b

    it should work for String out to 1000 long

    Example:

    1
   "011"  -> 3
   "010"  -> 2
   -----     -
   "101"  -> 5

    1
   "011"  -> 3
    "10"  -> 2
   -----     -
   "101"  -> 5

*/

class Grail1 {

    static class Pair {
        int carry;
        int position;
    }

    static Pair sumOnePosition (char first, char second, char carry) {
        int f = first - '0';
        int s = second - '0';
        int c = carry - '0';

        int total = f + s + c;

        Pair result = new Pair();
        result.position = total % 2;
        result.carry = total / 2;

        return result;
    }

    static String sum( String first, String second ) {
        if ( first.length() > second.length() ) {
            return sumLongFirst( first, second );
        } else {
            return sumLongFirst( second, first );
        }
    }

    static String sumLongFirst( String first, String second ) {

        char lastCarry = '0';
        String acumResult = "";

        for ( int i=0; i<first.length(); i++ ) {
            final int posA = first.length() - i - 1;
            final int posB = second.length() - i - 1;

            final char charA = first.charAt( posA );

            final char charB;
            if ( i < second.length() ) {
                charB = second.charAt( posB );
            } else {
                charB = '0';
            }

            final Pair posPair = sumOnePosition( charA, charB, lastCarry );
            acumResult = posPair.position == 0 ? "0" + acumResult : "1" + acumResult;
            lastCarry = posPair.carry == 0 ? '0' : '1';
        }

        return lastCarry == '0' ? acumResult : "1" + acumResult;
    }

    static void assertTest( String first, String second, String expect ) {
        String tmp = sum( first, second );
        if ( ! tmp.equals( expect ) ) {
            throw new RuntimeException("strings differ");
        }
    }

    static String createRepeated( char c, int repeatedTimes )
    {
        final StringBuilder buff = new StringBuilder();

        for (int i = 0; i < repeatedTimes; i++)
        {
            buff.append(c);
        }

        return buff.toString();
    }


    public static void main (String[] args) {
        assertTest( "011", "010", "101" );
        assertTest( "11", "010", "101" );
        assertTest( "010", "11", "101" );
        assertTest( "11", "10", "101" );
        assertTest( "0", "1", "1" );
        assertTest( "", "1", "1" );

        final String thousandOnes = createRepeated('1', 1000);
        final String thousandZeros = createRepeated('0', 1000);

        assertTest(thousandOnes, thousandZeros, thousandOnes);
        //  ( 2^k - 1 ) + ( 2^k - 1 ) = 2 * ( 2^k - 1 )
        assertTest(thousandOnes, thousandOnes, thousandOnes + "0");

        System.out.println("passed");
    }
}


