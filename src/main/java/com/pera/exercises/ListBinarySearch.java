package com.pera.exercises;

import java.util.Arrays;

class ListBinarySearch {

    static int binarySearch( int[] inArray, int inTarget ) {

        {
            int[] copy = Arrays.copyOf(inArray, inArray.length);
            Arrays.sort(copy);
            checkCondition(Arrays.equals(inArray, copy));
        }

        if ( inArray.length == 0 ) {
            return -1;
        }

        // len>1,
        // obtain first, median, and last index
        final int firstIndex = 0;
        final int lastIndex = inArray.length - 1;
        final int medianIndex = (firstIndex+lastIndex)/2;

        if ( inTarget < inArray[firstIndex] || inTarget > inArray[lastIndex]) {
            return -1;
        }
        else if ( inTarget == inArray[medianIndex] ) {
            return medianIndex;
        }
        else if ( inTarget > inArray[medianIndex] ) {
            final int tmp = binarySearch( sliceArray(inArray, medianIndex+1, lastIndex+1), inTarget);
            if ( tmp == -1 ) {
                return -1;
            }
            return (medianIndex+1) + tmp;
        }
        else { //  case of if ( inTarget < inArray[medianIndex] )
            return binarySearch( sliceArray(inArray, firstIndex, medianIndex+1), inTarget);
        }
    }

    static int[] sliceArray( int[] in, int firstIndexInclusive, int lastIndexExclusive) {
        checkCondition ( firstIndexInclusive < lastIndexExclusive );
        final int[] tmp = new int[lastIndexExclusive - firstIndexInclusive];
        System.arraycopy(
                in,
                firstIndexInclusive,
                tmp,
                0,
                lastIndexExclusive - firstIndexInclusive);
        return tmp;
    }

    static void checkCondition( boolean condition ) {
        if ( ! condition ) {
            throw new RuntimeException("");
        }
    }

    public static void main (String[] args) {
        ListBinarySearch.checkCondition( ListBinarySearch.binarySearch( new int[]{0,4,7,10}, 0 ) == 0 );
        checkCondition( binarySearch( new int[]{0,4,7,10}, 4 ) == 1 );
        checkCondition( binarySearch( new int[]{0,4,7,10}, 7 ) == 2 );
        checkCondition( binarySearch( new int[]{0,4,7,10}, 10 ) == 3 );
        checkCondition( binarySearch( new int[]{0,4,7,10}, 11 ) == -1 );
        checkCondition( binarySearch( new int[]{0,4,7,10}, -2 ) == -1 );
        //
        checkCondition( binarySearch( new int[]{0,4}, 0 ) == 0 );
        checkCondition( binarySearch( new int[]{0,4}, 4 ) == 1 );
        //
        checkCondition( binarySearch( new int[]{0}, 0 ) == 0 );
        checkCondition( binarySearch( new int[]{0}, 4 ) == -1 );

        System.out.println("passed");
    }
}
