package com.pera.exercises;

import java.util.*;

class LRUCache
{
    private final int maxSize;
    private final Map<String, CachedItem> map;
    private int lastSequence;

    private static class CachedItem {
        private final String value;
        private final int sequence;
        private CachedItem(int s, String v ) {
            value = v;
            sequence = s;
        }
    }

    public LRUCache( int inMaxSize )
    {
        lastSequence = 0;
        maxSize = inMaxSize;
        map = new HashMap<>();
    }

    public void set ( String key, String newValue ) {
        if (!map.containsKey(key)) {
            lastSequence++;
            if (map.size() >= maxSize) {
                map.remove(findOldest());
            }
        }
        map.put( key, new CachedItem( lastSequence, newValue ) );
    }

    private String findOldest() {

        int minimum = 0;
        boolean minimumIsSet = false;
        Map.Entry<String, CachedItem> minimumEntry = null;

        for (Map.Entry<String, CachedItem> entry : map.entrySet() ) {
            if ( entry.getValue().sequence < minimum || ! minimumIsSet )
            {
                minimum = entry.getValue().sequence;
                minimumIsSet = true;
                minimumEntry = entry;
            }
        }

        checkCondition( minimumEntry != null );

        return minimumEntry.getKey();
    }


    public String get ( String key ) {
        if ( map.containsKey( key ) ) {
            return map.get( key ).value;
        }
        else {
            return null;
        }
    }

    public void delete ( String key ) {
        map.remove( key );
    }

    static void checkCondition( boolean condition ) {
        if ( ! condition ) {
            throw new RuntimeException("");
        }
    }

    public static void main (String[] args) {

        final LRUCache cache = new LRUCache(3);

        cache.set( "1", "a" );
        cache.set( "2", "b" );
        cache.set( "3", "c" );
        checkCondition( cache.get( "4" ) == null );

        cache.set( "4", "d" );
        checkCondition( cache.get( "4" ) != null );

        cache.delete( "4" );
        checkCondition( cache.get( "4" ) == null );

        System.out.println("passed");
    }
}
