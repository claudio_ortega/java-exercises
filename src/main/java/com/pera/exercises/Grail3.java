package com.pera.exercises;

import java.util.*;

class Grail3 {

    final static int maxLevel = 11;
    final static int minLevel = 0;

    interface LevelListener {
        void levelChanged( int newLevel );
    }

    static class Switch {
        private final List<LevelListener> listeners;

        public Switch() {
            listeners = new LinkedList<>();
        }

        public boolean setLevel(int level) {
            if ( level < minLevel || level > maxLevel ) {
                return false;
            }

            for ( final LevelListener next : listeners ) {
                next.levelChanged( level );
            }

            return true;
        }

        public void addlistener( LevelListener l ) {
            listeners.add( l );
        }
    }

    static class LightBulb  implements  LevelListener {
        int level;
        double maxPower;
        double power;

        private void mappingLevelToPower() {
            power = level * maxPower / 11.0;
        }

        public LightBulb(float _maxPower, float _initialPower) {
            maxPower = _maxPower;
            power = _initialPower;
        }

        public void levelChanged(int _level) {
            if ( _level < minLevel || _level > maxLevel ) {
                throw new RuntimeException( "invariant not .." );
            }

            level = _level;
            mappingLevelToPower();
            System.out.println( String.format("power:%.2f", power));
        }
    }

    static void assertTest( boolean gotten, boolean expected ) {
        if ( gotten != expected ) {
            throw new RuntimeException("strings differ");
        }
    }

    public static void main (String[] args) {
        final Switch switchA = new Switch();
        final LightBulb downStairs = new LightBulb( 100.0f, 0f);
        final LightBulb upStairs = new LightBulb( 60.0f, 0f);

        switchA.addlistener( downStairs );
        switchA.addlistener( upStairs );

        assertTest( switchA.setLevel( 0 ), true );
        assertTest( switchA.setLevel( 3 ), true );
        assertTest( switchA.setLevel( 11 ), true );
        assertTest( switchA.setLevel( 12 ), false );
        System.out.println("passed");
    }
}


