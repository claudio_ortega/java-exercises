package com.pera.exercises.multithread;

import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

public class SomeConcurrentStuff {

    private ConcurrentLinkedDeque deque;
    private ConcurrentHashMap map;
    private ReentrantLock rl;

    public SomeConcurrentStuff() {
        deque = new ConcurrentLinkedDeque();
        map = new ConcurrentHashMap();
        rl = new ReentrantLock();
    }
}
