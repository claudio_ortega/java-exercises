package com.pera.exercises.multithread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

class CountDownLatchExample {
    public static void main (String[] args) throws InterruptedException {

        final java.util.concurrent.CountDownLatch latch = new java.util.concurrent.CountDownLatch(1);

        Thread t1 = new Thread(
            () -> {
                try {
                    Thread.sleep(5000);
                    latch.countDown();
                }
                catch (InterruptedException ex) {
                    System.out.println("oops");
                }
            }
        );

        t1.start();

        System.out.println( "waiting..." );
        final boolean didTimeout = !latch.await(15, TimeUnit.SECONDS);
        System.out.printf( "didTimeout:%b", didTimeout );
    }
}
