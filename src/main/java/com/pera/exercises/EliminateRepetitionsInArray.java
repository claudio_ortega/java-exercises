package com.pera.exercises;

import java.util.Arrays;

class EliminateRepetitionsInArray {

    static int eliminateRepetitions (int[] in) {
        int out_ix = 0;
        int previous = -1;
        for ( int in_ix=0; in_ix<in.length; in_ix++) {
            if ( in_ix == 0 || in[in_ix] != previous ) {
                in[out_ix] = in[in_ix];
                out_ix++;
            }
            previous = in[in_ix];
        }
        return out_ix;
    }

    static void assertTest( int[] underTest, int[] expected ) {
        int[] underTestCopy = Arrays.copyOf( underTest, underTest.length );
        int newLength = eliminateRepetitions( underTestCopy );
        int[] outCopy = Arrays.copyOf( underTestCopy, newLength );
        if ( !Arrays.equals ( outCopy, expected ) ) {
            throw new RuntimeException("arrays differ");
        }
    }

    public static void main (String[] args) {
        assertTest( new int[]{}, new int[]{} );
        assertTest( new int[]{1}, new int[]{1} );
        assertTest( new int[]{1,1,1}, new int[]{1} );
        assertTest( new int[]{1,8,9}, new int[]{1,8,9} );
        assertTest( new int[]{1,8,8,8,8,8,9,9,9}, new int[]{1,8,9} );
        System.out.println("passed");
    }
}

