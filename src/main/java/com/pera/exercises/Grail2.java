package com.pera.exercises;

import java.util.Arrays;

class Grail2 {

    static String compute ( String[] input, String reference ) {

        final StringBuilder concat = new StringBuilder();

        for (final String curr : input) {

            if (curr.equals("<")) {
                int currLength = concat.length();
                if (currLength > 0) {
                    concat.deleteCharAt(currLength - 1);
                }
            }

            else if (reference.contains(curr)) {
                concat.append(curr);
            }

            else {
                throw new RuntimeException(String.format("unexpected:[%s]", curr ) );
            }
        }

        return concat.toString();
    }

    static void assertTest( String[] inputA, String expected) {
        if ( !expected.equals( compute( inputA, "abcd" ) ) ) {
            throw new RuntimeException(String.format("compute failed, expected:%s, %s", expected, Arrays.asList(inputA) ) );
        }
    }

    public static void main (String[] args) {
        assertTest( new String[] {"a", "b", "c", "<"}, "ab" );
        assertTest( new String[] {"a", "b", "c", "<", "d"}, "abd" );
        assertTest( new String[] {"a", "b", "<", "c", "<", "d"}, "ad" );
        assertTest( new String[] {"a", "b", "<", "c", "<", "d", "<", "<"}, "" );
        assertTest( new String[] {""}, "" );
        assertTest( new String[] {"<"}, "" );
        assertTest( new String[] {"<", "<"}, "" );
        System.out.println("passed");
    }
}


